package com.atlassian.json.schema.annotation;

public class AnnotationHelper
{
    public static final String EMPTY_ENUM = "emptyEnum";
    public static final String EMPTY_TYPE = "emptyType";

    public static final class EmptyClass {}
}
