package com.atlassian.json.schema;

public enum SchemaType
{
    OBJECT ,INTEGER, NUMBER, BOOLEAN ,STRING, ARRAY;
}
