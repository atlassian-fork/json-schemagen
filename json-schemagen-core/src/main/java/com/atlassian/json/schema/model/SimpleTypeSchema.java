package com.atlassian.json.schema.model;

public class SimpleTypeSchema extends BasicSchema
{
    public SimpleTypeSchema(String type) 
    {
        setType(type);
    }
}
